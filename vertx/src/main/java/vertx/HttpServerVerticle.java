package vertx;

import java.io.ObjectOutputStream.PutField;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerRequest;

public class HttpServerVerticle extends AbstractVerticle {

	@Override
	public void start(Future<Void> startFuture) throws Exception {
		vertx.createHttpServer()
		.requestHandler(HttpServerRequest ->
		HttpServerRequest.response()
		.putHeader("content-type", "text/html")
		.end("Hello world !!")
		).listen(8082, httpServerAsyncResult -> {
			if (httpServerAsyncResult.succeeded()) {
				System.out.println("http sever started !!");
				startFuture.complete();
			}
			else startFuture.fail(httpServerAsyncResult.cause());
		});
	}

	
	
}
