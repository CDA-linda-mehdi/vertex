import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerRequest;


public class HelloWorld extends AbstractVerticle {

    @Override
    public void start() throws Exception {
        vertx.createHttpServer() 
            .requestHandler(req -> req.response() 
                .putHeader("content-type", "text/html") 
                .end("Hello World"))
            .listen(8082);
    }

	@Override
	public void start(final Future<Void> startFuture) throws Exception {
		vertx.createHttpServer()
		.requestHandler(req -> req.response()
				.putHeader("content-type", "text/html")
				.end("Hello toto"))
		.listen(8082, res -> {
			if (res.succeeded()) {
				startFuture.complete();
			} else {
				startFuture.fail(res.cause());
			}
		});
	}
	
	
    
    
}
