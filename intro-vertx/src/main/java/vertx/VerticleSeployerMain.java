package vertx;

import io.vertx.core.Vertx;

public class VerticleSeployerMain {

	public static void main(String[] args) {

		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(new HttpServerVerticle());
	}

}
