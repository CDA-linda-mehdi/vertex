import io.vertx.core.Vertx;

public class VerticleDeployerMain {

	public static void main(String[] args) throws Exception {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(new HttpServerVerticle());


	}

}
