import io.vertx.core.AbstractVerticle;

public class VerticleDeployer extends AbstractVerticle {

	@Override
	public void start() throws Exception {
		vertx.deployVerticle(new HttpServerVerticle());
	}
	
	

}
