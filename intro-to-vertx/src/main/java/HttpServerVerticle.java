import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;

public class HttpServerVerticle extends AbstractVerticle {

	@SuppressWarnings("deprecation")
	@Override
	public void start(Future<Void> startFuture) throws Exception {
		
		vertx.createHttpServer()
		.requestHandler(httpServerRequest ->
		httpServerRequest.response()
		.putHeader("content-type", "text/html")
		.end("Hello from Vert-x !!")
		).listen(8082, httpServerAsyncResult -> {
			if (httpServerAsyncResult.succeeded()) {
				System.out.println("Http server started !!");
				startFuture.complete();
			} 
			else startFuture.fail(httpServerAsyncResult.cause());
		});
	}

	@Override
	public void stop() throws Exception {
		System.out.println("Good Bye");
	}
	
	

}
