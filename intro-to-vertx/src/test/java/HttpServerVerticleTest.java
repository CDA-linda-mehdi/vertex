import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.vertx.core.Vertx;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;

public class HttpServerVerticleTest {

	private Vertx vertx;

	@Before
	public void init(TestContext testContext) {
		vertx = Vertx.vertx();
		vertx.deployVerticle(HttpServerVerticle.class.getName(), testContext.asyncAssertSuccess());
	}

	@After
	public void stopVerticles(TestContext testContext) {
		vertx.close(testContext.asyncAssertSuccess());
	}
	
	@Test
	public void testHttpServerVerticleWorksWell(TestContext testContext) {
		
		final Async async = testContext.async();
		
		vertx.createHttpClient().getNow(8080, "localhost", "/",
				response ->
		response.handler(body -> {
			testContext.assertTrue(body.toString().contains("Hello"));
			async.complete();
		}));
		
	}
}
